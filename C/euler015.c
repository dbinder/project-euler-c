/*********
* Dylan Binder
* Problem 15 - Project Euler
* https://projecteuler.net
*
* Which starting number under one million produces the longers chain?
* Starting in the top left corner of a 2×2 grid, and only being 
* able to move to the right and down, 
* there are exactly 6 routes to the bottom right corner.
* 
* How many such routes are there through a 20×20 grid?
*******/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int mallocArray(long int *** array, int nrows, int ncols){

	int i;
	*array = malloc(sizeof(long int *) * nrows);
	
	if(*array == NULL){
		printf("ERROR: out of Memory\n");
		return 1;

	}// end if

	for(i = 0; i < nrows; i++){
		(*array)[i] = malloc(sizeof(long int) * ncols);
		if((*array)[i] == NULL){
			printf("ERROR: out of Memory\n");
		}//end if

	}//end for
	printf("Allocated!\n");
	return 0;
	
}//end mallocArray

void zeroIt(long int ** array, int nrows, int ncols){
	
	int i,j;
	
	for(i = 0; i < nrows; i++){
		for(j = 0; j < ncols; j++)
			array[i][j] = 0;
	}//end for

	printf("Zeroed!!\n");

}//end zeroIt

int main(int argc, char **argv){


	long int **array;

	mallocArray(&array, 20, 20);
	
	long int num1, num2, num3;
	int i,j;
	//array[0][0] = 2;
		
	//printf("%li\n", array[0][0]);
	zeroIt(array, 20,20);
	
	i = j = 0;
	 clock_t t;
 FILE* fp;

 fp = fopen("../Data/timing_output.txt", "a")



	
	for(j = 0; j < 20; j++)
		array[i][j] = 2 + j; 
	
	
	
	j = 0;
	for(i = 0; i < 20; i++)
		array[i][j] = 2 + i; 

	
	
	for(i = 0; i < 19; i++)
	for(j = 1; j < 20; j++){
		num1 = array[i][j];
		num2 = array[i+1][j-1];
		num3 = num1 + num2;
		array[i+1][j] = num3;
		
		
	}
	 t = clock() - t;
 fclose(fp, "Problem 1 took %0.9f ms in C\n", t);
 printf("Sum is %d", sum);
	printf("%li\n", array[19][19]);
	return 0;
}//end main
