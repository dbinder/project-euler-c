/*********
* Dylan Binder
* Problem 5 - Project Euler
* https://projecteuler.net
*
* 2520 is the smallest number that can be divided 
* by each of the numbers from 1 to 10 without any remainder.
* What is the smallest positive number that is evenly 
* divisible by all of the numbers from 1 to 20?
**********/



#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 20

int gcd(int a, int b){
		if(b == 0){
			return a;
		}
		return gcd(b, a % b);
	
}

int main(int argc, char **argv){
	
    int r = 1;
	int  i,j;

	 clock_t t;
 FILE* fp;

 fp = fopen("../Data/timing_output.txt", "a")
	for(i = 2; i <= N; ++i )
      if(j = r % i)
        r *= i / gcd(i, r);

     t = clock() - t;
 fclose(fp, "Problem 1 took %0.9f ms in C\n", t);
 printf("Sum is %d", sum);
	printf( "\n%d\n", r );

	return 0;
}




