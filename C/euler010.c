/*********
* Dylan Binder
* Problem 10 - Project Euler
* https://projecteuler.net
*
* The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
* Find the sum of all the primes below two million.
**********/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/*
* Bleh this problem sucked on windows, Pretty easy on Linux. 
* compile with 
* gcc -std=c99 -o euler010 euler.c
*/

unsigned long long int  sieve(unsigned long max){

	int *sieveArray = (int *)malloc(max * sizeof(int));
	int iter, iter2,  result;
    long long int sum;
	
	iter=0;
	while( iter < max){
		if (iter == 1)
			sieveArray[iter] = 0;
		else if(iter == 2)
			sieveArray[iter] = 2;
		else if(iter % 2 == 0)
			sieveArray[iter] = 0;
			else 
			sieveArray[iter] = iter;		
	   
	 iter++;
	}
   
   //begin seive
   iter = 3;
   for(iter; iter <= sqrt(max); iter++){
             
   result = checkPrime(iter);
          if(result == 1){
                    iter2 = iter + 1;
                    for(iter2; iter2 <= max; iter2++)
                               if(iter2 % iter == 0){
                                        sieveArray[iter2] = 0;          				
				}                    
                
          }
             
             
   } 
   
   iter = 0;
	sum=0;
   while(iter < max)
   {
    sum += sieveArray[iter];
    iter++;   
     
   }
   free(sieveArray);
   
   return sum;   

}
//Check prime return 0 if not 1 if the number is a prime
int checkPrime(int a){
    
    int i;

    for (i = 2;  i <= a - 1; i++){
        
        if (a % i == 0)
           return 0;       
    }    
    if(i == a)
         return 1;
    
    
}

int main(int argc, char **argv){

	int count, i = 3, c;
	long long int sum;

	sum = 0;
 clock_t t;
 FILE* fp;

 fp = fopen("../Data/timing_output.txt", "a")


 
	sum = sieve(2000000);
	printf("%lli\n", sum);
  t = clock() - t;
 fclose(fp, "Problem 1 took %0.9f ms in C\n", t);
 printf("Sum is %d", sum);
    return 0;
}//end main
