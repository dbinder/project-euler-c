/*********
* Dylan Binder
* Problem 17 - Project Euler
* https://projecteuler.net
*
* If the numbers 1 to 5 are written out in words: 
* one, two, three, four, five, 
* then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
* 
* If all the numbers from 1 to 1000 (one thousand)
* inclusive were written out in words, how many letters would be used?
*  
* NOTE: Do not count spaces or hyphens. 
* For example, 342 (three hundred and forty-two) 
* contains 23 letters and 115 (one hundred and fifteen) 
* contains 20 letters. The use of "and" when writing out 
* numbers is in compliance with British usage.
*******/


#include <stdio.h>
#include <time.h>


int main(int argc, char **argv){


	int one [20] = {0,3,3,5,4,4,3,5,5,4,3,6,6,8,8,7,7,9,8,8};
	int tens[10]  = {0,3,6,6,5,5,5,7,6,6};
	
	int i,a,b,c,h,t, sum = 0;
	h = 7;//hundred
	t = 8;//thousand
 clock_t t;
 FILE* fp;

 fp = fopen("../Data/timing_output.txt", "a")



	
	for(i = 1; i <= 1000; i++){
		a = i % 10;//singles digit
		b = ((i % 100) - a)/10;//tens digit
		c = ((i % 1000) - (b*10) - a)/100;//hundreds digit
		
		if (c != 0){
			sum += (one[c] + h);

			if (b != 0 || a != 0)
				sum += 3; // "and"
		}
		if(b==0 || b == 1)
			sum += one[b * 10 +c];
		else
			sum += tens[b] +one[a];
		
	}
	sum += (one[1] + t);
	printf("%d\n", sum);
	 t = clock() - t;
 fclose(fp, "Problem 1 took %0.9f ms in C\n", t);
 printf("Sum is %d", sum);

	return 0;
}//end main

