/*********
* Dylan Binder
* Problem 8 - Project Euler
* https://projecteuler.net
*
* Find the greatest product of five consecutive digits in the 1000-digit number.
* 73167176531330624919225119674426574742355349194934
* 96983520312774506326239578318016984801869478851843
* 85861560789112949495459501737958331952853208805511
* 12540698747158523863050715693290963295227443043557
* 66896648950445244523161731856403098711121722383113
* 62229893423380308135336276614282806444486645238749
* 30358907296290491560440772390713810515859307960866
* 70172427121883998797908792274921901699720888093776
* 65727333001053367881220235421809751254540594752243
* 52584907711670556013604839586446706324415722155397
* 53697817977846174064955149290862569321978468622482
* 83972241375657056057490261407972968652414535100474
* 82166370484403199890008895243450658541227588666881
* 16427171479924442928230863465674813919123162824586
* 17866458359124566529476545682848912883142607690042
* 24219022671055626321111109370544217506941658960408
* 07198403850962455444362981230987879927244284909188
* 84580156166097919133875499200524063689912560717606
* 05886116467109405077541002256983155200055935729725
* 71636269561882670428252483600823257530420752963450
**********/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>




int product(char *p){
	int products, i;

	products = 1;
	//char myArray[5];
	//myArray[0] = *p;	
	for(i = 0; i <=4; i++){
		if((p[i]-48) == 0){	
			return 0;

		}else{
			//printf("%d", ((p[i])-48));
		products *= ((p[i])-48);
		}
	}
		

	return products;

}


int main(int argc, char **argv){

	int count;
	 clock_t t;
	 FILE* fp;

 	fp = fopen("../Data/timing_output.txt", "a")


 

	if(argc != 2) {
		printf("usage: %s filename", argv[0]);

	}
	else{	
		FILE *fp=fopen(argv[1], "r");
		
		if(fp == 0){
			printf("Could not opent file\n");		
		}else{
			int x, count, largest, temp;
			count = largest = temp = 0; 
			char myArray[5] ={'0','0','0','0','0'};		
			
			while ((x = fgetc(fp)) != EOF){
				
				int i;
				if(count <= 4){
				myArray[count] = x;
				}
				
				
				if(count > 4 && x != '\n'){
				myArray[0]=myArray[1];
				myArray[1]=myArray[2];
				myArray[2]=myArray[3];
				myArray[3]=myArray[4];
				myArray[4]= x;
				//for(i= 0; i <= 4; i++)
				//printf("%d", myArray[i]-48);
				}
				
				
				if(product(myArray) == 0){
					;
				}else{
				 	temp = product(myArray);
				 	if(temp > largest)
						largest = temp;
						
					
	
				}
				
				count++;
				
			}	
			printf("%d\n", largest);	
			fclose(fp);	
			}								
							
		}		
	
		t = clock() - t;
 	fclose(fp, "Problem 1 took %0.9f ms in C\n", t);
	 printf("Sum is %d", sum);

	

	return 0;

}//end main
