/*********
* Dylan Binder
* Problem 7 - Project Euler
* https://projecteuler.net
*
* By listing the first six prime numbers: 
* 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
* What is the 10001st prime number?
**********/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main(int argc, char **argv){

	int count, i = 3, c;
	double n;	
	n = 0;
		
	argc = atoi(argv[1]);

 clock_t t;
 FILE* fp;

 fp = fopen("../Data/timing_output.txt", "a")



	if ( argc >= 1){
		printf("First %d prime numbers are :\n", argc);
		printf("2\n");	
	}

	for(count = 2; count <= argc;){
		for(c = 2; c <= i -1; c++){
			if(i%c==0)
				break;
		}
		if(c == i){
			printf("%d\n", i);
			count++;		
			n += i;	
		}
		i++;
	}

	t = clock() - t;
 	fclose(fp, "Problem 1 took %0.9f ms in C\n", t);
	printf("Sum is %d", sum);
	printf("%d\n", n);
	return 0;
}//end main
