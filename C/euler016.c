/*********
* Dylan Binder
* Problem 16 - Project Euler
* https://projecteuler.net
*
* 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
* 
* What is the sum of the digits of the number 2^1000?
*******/


#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>



int main(int argc, char **argv){
	FILE *fp;
	int x, sum = 0;
	 clock_t t;
	 FILE* fp;

 	fp = fopen("../Data/timing_output.txt", "a")



	fp = fopen("Data//euler016.txt", "w");
	if(fp == NULL)
		perror("Error Opening Files");
	double number[1];
	number[0] = pow(2,1000);
	fprintf(fp, "%.f\n", number[0] );	
	printf("%.f\n", number[0]);
	fclose(fp);
	fp = fopen("Data//euler016.txt", "r");
	while((x = fgetc(fp)) != EOF){
		if(x != '\n')
		sum = sum + (x - 48);
		
	}	
	printf("%d\n", x + 48);
	printf("%d\n", sum);
	t = clock() - t;
	fclose(fp, "Problem 1 took %0.9f ms in C\n", t);
	printf("Sum is %d", sum);
	fclose(fp);
    system("pause");
	//printf("%f\n", sum);
}//end main
