/*********
* Dylan Binder
* Problem 3 - Project Euler
* https://projecteuler.net
*
* The prime factors of 13195 are 5,7,13 and 29
* what is the largest prime factor of the number 600851475143
**********/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


long largestPrimeFactor(long n){
	long largestFactor = 1;
	long p = 3;
	long div = n;

	while(div % 2 == 0){
		largestFactor = 2;
		div = div / 2;
	}

	while(div != 1){
		while(div % p == 0){
			largestFactor = p;
			div = div / p;
		}
		p = p + 2;
	}
	
	return largestFactor;
}

int main(int argc, char **argv){
    
 clock_t t;
 FILE* fp;

 fp = fopen("../Data/timing_output.txt", "a")
  printf("%ld\n", largestPrimeFactor(600851475143));	

  
  t = clock() - t;
 fclose(fp, "Problem 1 took %0.9f ms in C\n", t);
 printf("Sum is %d", sum);

  return 0;
}

