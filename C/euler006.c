/*********
* Dylan Binder
* Problem 6 - Project Euler
* https://projecteuler.net
*
* The sum of the squares of the first ten natural numbers is,
* 1^2 + 2^2 + ... + 10^2 = 385
* The square of the sum of the first ten natural numbers is,
* (1 + 2 + ... + 10)^2 = 55^2 = 3025
* Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
* Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
**********/
#include <stdio.h>
#include <time.h>




int main(int argc, char **argv){
	int i, sum1, sum2, diff;	
	 clock_t t;
 FILE* fp;

 fp = fopen("../Data/timing_output.txt", "a")


 
	sum1 = sum2 = 0;
	for (i = 1; i <=100; i++){
		sum1 = sum1 + i*i;
		sum2 = sum2 + i;

		
	}

	sum2 = sum2 * sum2;

	diff = sum2 - sum1;
	t = clock() - t;
 	fclose(fp, "Problem 1 took %0.9f ms in C\n", t);
 	printf("Sum is %d", sum);
	printf("%d is the difference", diff);	
		

	return 0;
}//end main
