/*********
* Dylan Binder
* Problem 9 - Project Euler
* https://projecteuler.net
*
* A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
* a^2 + b^2 = c^2
* For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
* There exists exactly one Pythagorean triplet for which a + b + c = 1000.
* Find the product abc.
**********/

#include <stdio.h>
#include "euler.h"
#import <time.h>
/*******
* Pythagorean Triples	
* Euclids formula
* a = m*m - n*n, b = 2mn, c = m*m +n*n
*  m and n k positive intergers and m > n m-n = odd, with m and m coprime
********/


int main(int argc, char **argv){

	int a,b,c,m,n, sum, coprime;
	n = sum = 0;
	 clock_t t;
 FILE* fp;

 fp = fopen("../Data/timing_output.txt", "a")




	for(m = 0; m < 25; m++){
			
		coprime = gcd(m,n);
		if((m - n) % 2 != 0 && coprime == 1)
			for(n = 1; n < m; n++){
		
				a = (m*m - n*n);
				b = (2*m*n);
				c = ((m*m) + (n*n));
				sum = a+b+c;

				if(sum == 1000){
				printf("%d = sum\n%d = a\n%d = b\n%d = c\n%d = m\n%d = n\n", sum, a,b,c,m,n);
				printf("%d = product\n", a*b*c);
				}
	
					
			}	

	}

	 t = clock() - t;
 	fclose(fp, "Problem 1 took %0.9f ms in C\n", t);
 	printf("Sum is %d", sum);
	printf("%d = sum\n%d = a\n%d = b\n%d = c\n%d = m\n%d = n\n", sum, a,b,c,m,n);

return 0;
}//end main
