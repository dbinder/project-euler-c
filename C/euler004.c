/*********
* Dylan Binder
* Problem 4 - Project Euler
* https://projecteuler.net
*
* A palindromic number reads the same both ways. 
* The largest palindrome made from the product of 
* two 2-digit numbers is 9009 = 91 × 99.
* Find the largest palindrome made from the product of two 3-digit numbers.
**********/



#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int isPalindrome(int num){
	int reversed = 0;
	int original = num;

	if(num < 10) return 1;
	if(num % 10 == 0) return 0;

	while(num >= 1){
		reversed = (reversed * 10) + (num % 10);
		num = num/10;	
	}

	if(original == reversed) return 1;
	else return 0;
	
}

int main(int argc, char **argv){
	int maxPalindrome = 0;
	int a, b, product;
	
	 clock_t t;
    FILE* fp;

    fp = fopen("../Data/timing_output.txt", "a")
	a = 999;
	while(a > 99){
		b = 999;
		while(b >= a){
			product = a*b;
			if(product > maxPalindrome && isPalindrome(product))
				maxPalindrome = product;
			b--;
		}	
		a--;
	}

	   t = clock() - t;
  	fclose(fp, "Problem 1 took %0.9f ms in C\n", t);
	 printf("Sum is %d", sum);
	printf("Found the Max Palidrome %d\n", maxPalindrome);
}
