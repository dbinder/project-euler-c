/*********
* Dylan Binder
* Problem 1 - Project Euler
* https://projecteuler.net
*
* If we list all the natural numbers below 10
* that are multiples of 3 or 5, we get 3, 5, 6 
* and 9. The sum of these multiples is 23.
* Find the sum of all the multiples of 3 or 5 below 1000.
*********/


#include <stdio.h>
#include <time.h>

int main(int argc, char *argv[]){
	
 int sum, i;
 sum = 0;
 clock_t t;
 FILE* fp;

 fp = fopen("../Data/timing_output.txt", "a")

 t = clock();
 for(i = 0; i < 1000; i++){
	if(i % 3 == 0 || i % 5 == 0)
	  sum += i;
 }
 t = clock() - t;
 fclose(fp, "Problem 1 took %0.9f ms in C\n", t);
 printf("Sum is %d", sum);
 
return 0;
}
