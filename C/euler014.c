/*********
* Dylan Binder
* Problem 14 - Project Euler
* https://projecteuler.net
*
* The following iterative sequence is defined for the set of positive integers:
* 
* n → n/2 (n is even)
* n → 3n + 1 (n is odd)
* 
* Using the rule above and starting with 13, we generate the following sequence:
* 
* 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
* It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. 
* Although it has not been proved yet (Collatz Problem), 
* it is thought that all starting numbers finish at 1.
* 
* Which starting number, under one million, produces the longest chain?
* 
* NOTE: Once the chain starts the terms are allowed to go above one million.
*******/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int Collatz(unsigned long max){
   
    int *collatz_length = (int *)malloc(max* sizeof(int));
	int TO_ADD[600];
	unsigned long iter, j, m,n,s,p,ind,new_length, max_length = 0;
  iter = 0;
  
	while(iter<max){
        collatz_length[iter] = 0;                
        iter++;
    }
    
    collatz_length[1]=1;
    iter=0;
    while(iter < 600){
        TO_ADD[iter]=0;
        iter++;           
    }

    iter = 1;
    while(iter < max){
               n = iter;
               s = 0;
               
               while(n > max - 1 || collatz_length[n] < 1){
                       TO_ADD[s] = n;
                       	if(n % 2 == 0)
	                    	n = n / 2;
                      	else
                       		n = 3*n +1;
                       s++;
               
               }           
               p = collatz_length[n];
               j = 0;
               while(j<s){
               m = TO_ADD[j];
               TO_ADD[j] = 0;
               
               if(m < max){
                    new_length = collatz_length[n] + s - j;
                    collatz_length[m] = new_length;
                    if(new_length > max_length){
                                  max_length = new_length;
                                  ind = m;
                    }
               
                    
               }
               j++;
                          
               }
               iter++;
    }	
    free(collatz_length);
	return ind;

}


int add(int x, int y)
{
 return x+y;    
}

int main(int argc, char **argv){
   clock_t t;
 FILE* fp;

 fp = fopen("../Data/timing_output.txt", "a")



	int i, count, n;
  	
    n = 13;

	count = Collatz(1000000);
	//count = 0;
	n = add(9,30);
	printf("%d=add \n", n);
	printf("%d=count\n", count);
   t = clock() - t;
 fclose(fp, "Problem 1 took %0.9f ms in C\n", t);
 printf("Sum is %d", sum);
  
    system("pause");
}//end main
