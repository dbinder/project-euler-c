for x in range(2, 19):
    if (x < 10):
        path = "CPP/euler00%d.cpp" %x 
    else:
        path = "CPP/euler0%d.cpp" %x

    f = open(path, 'w')
    f.write("/*********\n* Dylan Binder\n* Problem %d - Project Euler\n" % x)
    f.write("* https://projecteuler.net" )
    f.write('*\n' + '*' * 9 + '/')
    f.close()
