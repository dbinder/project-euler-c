#Generate Files for Project Euler python
for x in range(5, 19):
    if (x < 10):
        path = "Python/euler00%d.py" %x 
    else:
        path = "Python/euler0%d.py" %x

    f = open(path, 'w')
    f.write("# \n# Dylan Binder\n# Problem %d - Project Euler\n" % x)
    f.write("# https://projecteuler.net" )
    f.write('#\n' + '#')
    f.close()