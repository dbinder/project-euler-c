#TODO

1. Add timing and write all the data to a file 
2. Fix data paths to the moved C code
3. Make euler.h more usable.
4. Clean up euler019.py.
5. Clean up euler020.py.
6. Clean up euler022.py.
7. Clean up euler048.py.
8. Clean up euler044.py
9. Clean up euler092.py
10. Clean up euler012.c.
11. Fix timing code in the C files, the stubbed out timing will break things.  
12 Put timing code in euler.h maybe. 



#DONE
1. Write a Python script to stub out my files. 
2. Write the problem in the code as a comment. 
	1. Done for C files. 
3. Stubbed out all timing code in the C files. 
4. Finished problem 19 in python. 
5. Finished problem 20 in python. 
6. Finished problem 22 in python.
7. Finished problem 44, and 48, 92 in python.  

