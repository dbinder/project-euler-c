/******
* Dylan Binder
* Problem 2
******/




/******************************/
#include <iostream>

int fibonacciRecursive();

int fibonacciRecursive(int n) {
if (n < 2) 
    return n;
else
    return fibonacciRecursive(n - 1) + fibonacciRecursive(n - 2);
}

int main(int argc, char *argv[]){

  int i, sum;
  i = 0;
  sum = 0;

	
	while(fibonacciRecursive(i) < 4000000)
	{
		if(fibonacciRecursive(i) % 2 == 0){
			sum = sum + fibonacciRecursive(i);
			
		}
		i++;	
	}
	
   std::cout << fibonacciRecursive(i) << std::endl;
   std::cout <<  sum;

	return 0;
}


