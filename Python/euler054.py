# 
# Dylan Binder
# Problem 54 - Project Euler
# https://projecteuler.net#

#In the card game poker, a hand consists of five cards and are ranked, from lowest to highest, in the following way:
#
#High Card: Highest value card.
#One Pair: Two cards of the same value.
#Two Pairs: Two different pairs.
#Three of a Kind: Three cards of the same value.
#Straight: All cards are consecutive values.
#Flush: All cards of the same suit.
#Full House: Three of a kind and a pair.
#Four of a Kind: Four cards of the same value.
#Straight Flush: All cards are consecutive values of same suit.
#Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.
#The cards are valued in the order:
#2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace.
#
#If two players have the same ranked hands then the rank made up of the highest value wins; for example, a pair of eights beats a pair of fives (see example 1 below). But if two ranks tie, for example, both players have a pair of queens, then highest cards in each hand are compared (see example 4 below); if the highest cards tie then the next highest cards are compared, and so on.
#
#Consider the following five hands dealt to two players:
#
#Hand 	Player 1	 	  Player 2	 		 Winner
#1	 	5H 5C 6S 7S KD 	  2C 3S 8S 8D TD 	 Player 2
#		Pair of Fives     Pair of Eights

#2	 	5D 8C 9S JS AC 	  2C 5C 7D 8S QH 	 Player 1
# 		Highest card Ace  Highest card Queen
 	
#3	 	2D 9C AS AH AC    3D 6D 7D TD QD
#		Three Aces 		  Flush with Diamonds Player 2

#4	 	4D 6S 9H QH QC 	  3D 6D 7H QD QS
#		Pair of Queens 	  Pair of Queens
#		Highest card Nine Highest card Seven Player 1

#5	 	2H 2D 4C 4D 4S 	  3C 3D 3S 9S 9D
#		Full House 		  Full House
#		With Three Fours  With Three Threes  Player 1
 
#The file, poker.txt, contains one-thousand random hands dealt to two players. Each line of the file contains ten cards (separated by a single space): the first five are Player 1's cards and the last five are Player 2's cards. You can assume that all hands are valid (no invalid characters or repeated cards), each player's hand is in no specific order, and in each hand there is a clear winner.

#How many hands does Player 1 win?
CARDS = "23456789TJQKA"
CARDS_VALUES = dict(zip(CARDS, range(2,15)))

class Card(object):
	def __init__(self, value = 0, suit = None):
		#2,3,4,5,6,7,8,9,10 11 = Jack, 12 = Queen, 13 = King, 14 = Ace
		self.value = value
		#Diamond, Club, Heart, Spade
		self.suit = suit

	def __str__(self):
		rep = "Value = ", self.value,  "Suit = ", self.suit
		return rep 

	def __lt__(self, other):
		return self.value < other.value


	
def handlist(hand):
		handlist = [Card() for x in range(5)]
		for x in range(5):
			temp = hand[x]
			v = temp[0:1]
			s = temp[1:2]
			s = suit(s)
			handlist[x].value = CARDS_VALUES.get(v)
			handlist[x].suit = s 
		
		return handlist
	
	

def suit(n):
	if n == "S":
		return "Spade"
	elif n == "D":
		return "Diamond"
	elif n == "H":
		return "Heart"
	else: return "Club"		
	

g_test = 0





def comepareHighCards(hand1, hand2):
	# return the Hand with the highest card
	
	
	card1 = highCard(hand1)
	card2 = highCard(hand2)
	if(int(card1.value) > int(card2.value)):
		return hand1
	elif(int(card1.value) < int(card2.value)): 
		return hand2
	
def lowCard(hand):
	temp = 15;
	for x in range(5):
		card = hand[x]
		i = int(card.value)
		if i < temp:
			temp = i
			cardReturn = hand[x]
	return cardReturn

def highCard(hand):
	#return high card
	#card object will always be greater than zero
	temp = 0;
	for x in range(5):
		card = hand[x]
		i = int(card.value)
		if i > temp:
			temp = i
			cardReturn = hand[x]
	return cardReturn
def isOnePair(hand):
	#compare card 1 to 2,3,4,5
	#compare card 2, to 3,4,5
	#compare card 3, to 4, 5
	#compare care 4, to 5
	#return True, False otherwise. 
	card_one 	= hand[0]
	card_two 	= hand[1]
	card_three 	= hand[2]
	card_four	= hand[3]
	card_five	= hand[4]

	if (card_one.value == card_two.value or card_one.value == card_three.value or card_one.value == card_four.value  or card_one.value == card_five.value):
		return True
	elif (card_two.value == card_three.value or card_two.value == card_four.value  or card_two.value == card_five.value):
		return True
	elif (card_three.value == card_four.value  or card_three.value == card_five.value):
		return True
	elif (card_four.value == card_five.value):
		return True
	else:
		return False
def isTwoPair(hand):
	#cases
	#1 == 2
	#1 == 3
	#1 == 4
	#1 == 5
	#if 1 == 2
	#then only 3 == 4 or 3 == 5 or 4 == 5
	#if 1 == 3 
	#then only 2 == 4 or 2 == 5 or 4 == 5
	#if 1 == 4
	# then only 2 == 3 or 2 == 5 or 3 == 5
	#if 1 == 5
	# then only 2 == 3 or 2 == 4 or 3 == 4
	#if 2 == 3
	#then only 4 == 5
    #['3C', '3D', '3H', '4S', '6C']
	card_one 	= hand[0]
	card_two 	= hand[1]
	card_three 	= hand[2]
	card_four	= hand[3]
	card_five	= hand[4]
	if(card_one.value == card_two.value):
		if(card_three.value == card_four.value or card_three.value == card_five.value or card_four.value == card_five.value):
			return True
		else:
			return False
	elif(card_one.value == card_three.value):
		if(card_two.value == card_four.value or card_two.value == card_five.value or card_four.value == card_five.value):
			return True
		else:
			return False
	elif(card_one.value == card_four.value):
		if(card_two.value == card_three.value or card_two.value == card_five.value or card_three.value == card_five.value):
			return True
		else:
			return False
	elif(card_one.value == card_five.value):
		if(card_two.value == card_three.value or card_two.value == card_four.value or card_three.value == card_four.value):
			return True
		else:
			return False
	elif(card_two.value == card_three.value):
		if(card_four.value == card_five.value):
			return True
		else:
			return False
	else:
		return False

def isThreeOfAKind(hand):
	#Cases
	#1 == 2 == 3
	#1 == 2 == 4
	#1 == 2 == 5
	#1 == 3 == 4
	#1 == 3 == 5
	#1 == 4 == 5
	#2 == 3 == 4
	#2 == 3 == 5
	#3 == 4 == 5

	card_one 	= hand[0]
	card_two 	= hand[1]
	card_three 	= hand[2]
	card_four	= hand[3]
	card_five	= hand[4]
	if (card_one.value == card_two.value == card_three.value):
		return True
	elif (card_one.value == card_two.value == card_four.value):
		return True
	elif (card_one.value == card_two.value == card_five.value):
		return True
	elif (card_one.value == card_three.value == card_four.value):
		return True
	elif (card_one.value == card_three.value == card_five.value):
		return True
	elif (card_one.value == card_four.value == card_five.value):
		return True
	elif (card_two.value == card_three.value == card_four.value):
		return True
	elif (card_two.value == card_three.value == card_five.value):
		return True
	elif (card_three.value == card_four.value == card_five.value):
		return True
	else:
		return False
def isStraight(hand):
	#hand has to comes in sorted
	card_one 	= hand[0]
	card_two 	= hand[1]
	card_three 	= hand[2]
	card_four	= hand[3]
	card_five	= hand[4]
	
	one = int(card_one.value)
	two = int(card_two.value)
	three = int(card_three.value)
	four = int(card_four.value)
	five = int(card_five.value)

	if(two == (one + 1) and three == (one + 2) and four == (one +3) and five == (one + 4)):
		return True
	else:
		return False

def isFlush(hand):
	#return True if Flush
	card_one 	= hand[0]
	card_two 	= hand[1]
	card_three 	= hand[2]
	card_four	= hand[3]
	card_five	= hand[4]
	
	if(card_one.suit == card_two.suit == card_three.suit == card_four.suit == card_five.suit):
		return True
	else:
		return False


def largerFullhouse(hand1, hand2):
	#only gets called if both hands are ruled a Full House
	#Returns a Hand
	h1 = highCard(hand1)
	l1 = lowCard(hand1)
	h2 = highCard(hand2)
	l2 = lowCard(hand2)
	card1_one 		= hand1[0]
	card1_two 		= hand1[1]
	card1_three 	= hand1[2]
	card1_four		= hand1[3]
	card1_five		= hand1[4]

	card2_one 		= hand2[0]
	card2_two 		= hand2[1]
	card2_three 	= hand2[2]
	card2_four		= hand2[3]
	card2_five		= hand2[4]

	#Check if there are 3
	s = h1.value
	t = l1.value
	print(s)
	if (s == card1_one.value == card1_two.value == card1_three.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value > l2.value):
			return hand1
		else:
			return hand2
	elif (s == card1_one.value == card1_two.value == card1_four.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value > l2.value):
			return hand1
		else:
			return hand2
	elif (s == card1_one.value == card1_two.value == card1_five.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value > l2.value):
			return hand1
		else:
			return hand2
	elif (s == card1_one.value == card1_three.value == card1_four.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value > l2.value):
			return hand1
		else:
			return hand2
	elif (s == card1_one.value == card1_three.value == card1_five.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value > l2.value):
			return hand1
		else:
			return hand2
	elif (s == card1_one.value == card1_four.value == card1_five.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value > l2.value):
			return hand1
		else:
			return hand2
	elif (s == card1_two.value == card1_three.value == card1_four.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value > l2.value):
			return hand1
		else:
			return hand2
	elif (s == card1_two.value == card1_three.value == card1_five.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value > l2.value):
			return hand1
		else:
			return hand2
	elif ( s == card1_three.value == card1_four.value == card1_five.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value > l2.value):
			return hand1
		else:
			return hand2
	elif (t == card1_one.value == card1_two.value == card1_three.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value > l2.value):
			return hand1
		else:
			return hand2
	elif (t == card1_one.value == card1_two.value == card1_four.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value > l2.value):
			return hand1
		else:
			return hand2
	elif (t == card1_one.value == card1_two.value == card1_five.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value > l2.value):
			return hand1
		else:
			return hand2
	elif (t == card1_one.value == card1_three.value == card1_four.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value > l2.value):
			return hand1
		else:
			return hand2
	elif (t == card1_one.value == card1_three.value == card1_five.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value > l2.value):
			return hand1
		else:
			return hand2
	elif (t == card1_one.value == card1_four.value == card1_five.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value > l2.value):
			return hand1
		else:
			return hand2
	elif (t == card1_two.value == card1_three.value == card1_four.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value > l2.value):
			return hand1
		else:
			return hand2
	elif (t == card1_two.value == card1_three.value == card1_five.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value> l2.value):
			return hand1
		else:
			return hand2
	elif (t == card1_three.value == card1_four.value == card1_five.value):
		if (h1.value > h2.value):
			return hand1
		elif(l1.value > l2.value):
			return hand1
		else:
			return hand2
def isFullHouse(hand):
	if(isThreeOfAKind(hand) and isOnePair(hand)):
		return True
	else:
		return False

def isFourOfAKind(hand):
	# 1 == 2 == 3 == 4
	# 1 == 3 == 4 == 5 
	# 1 == 2 == 4 == 5
	# 1 == 2 == 3 == 5
	# 2 == 3 == 4 == 5

	card_one 	= hand[0]
	card_two 	= hand[1]
	card_three 	= hand[2]
	card_four	= hand[3]
	card_five	= hand[4]
	
	if (card_one.value == card_two.value == card_three.value == card_four.value):
		return True
	elif (card_one.value == card_three.value == card_four.value == card_five.value):
		return True
	elif (card_one.value == card_two.value == card_four.value == card_five.value):
		return True
	elif (card_one.value == card_two.value == card_three.value == card_five.value):
		return True
	elif (card_two.value == card_three.value == card_four.value == card_five.value):
		return True
	else:
		return False
	
def isStraighFlush(hand):
	if(isStraight(hand) and isFlush(hand)):
		return True
	else:
		return False

def valueOfPair(hand):
	#FIX This
	count = 0
	card_one 	= hand[0]
	card_two 	= hand[1]
	card_three 	= hand[2]
	card_four	= hand[3]
	card_five	= hand[4]
	high_card = highCard(hand)
	
	if(int(high_card.value) == int(card_one.value)):
		count += 1

	if(int(high_card.value) == int(card_two.value)):
		count += 1
	
	if(int(high_card.value) == int(card_three.value)):
		count += 1
	
	if(int(high_card.value) == int(card_four.value)):
		count += 1
	
	if(int(high_card.value) == int(card_five.value)):
		count += 1

	
	if (count >= 2):
		return int(high_card.value)

	if (card_one.value == card_two.value or card_one.value == card_three.value or card_one.value == card_four.value  or card_one.value == card_five.value):
		return int(card_one.value)		
	elif (card_two.value == card_three.value or card_two.value == card_four.value  or card_two.value == card_five.value):
		return int(card_two.value)
	elif (card_three.value == card_four.value  or card_three.value == card_five.value):
		return int(card_three.value)
	elif (card_four.value == card_five.value):
		return int(card_four.value)
	else:
		return 0



def isRoyalFlush(hand):
	card_one 	= hand[0]
	card_two 	= hand[1]
	card_three 	= hand[2]
	card_four	= hand[3]
	card_five	= hand[4]

	if(isFlush(hand)):
		if(card_one.value == 10 and isStraight(hand)):
			return True
		else:
			return False
	else:
		return False


def compareHands(hand1, hand2):
	high_card_hand 	= comepareHighCards(hand1, hand2) 
	rFlush1		= isRoyalFlush(hand1)
	rFlush2 	= isRoyalFlush(hand2)
	fKind1 		= isFourOfAKind(hand1)
	fKind2 		= isFourOfAKind(hand2)
	sFlush1 	= isFlush(hand1)
	sFlush2 	= isFlush(hand2)
	fHouse1 	= isFullHouse(hand1)
	fHouse2 	= isFlush(hand2)
	straight1 	= isStraight(hand1)
	straight2 	= isStraight(hand2)
	tKind1 		= isThreeOfAKind(hand1)
	tKind2 		= isThreeOfAKind(hand2)
	tPair1 		= isTwoPair(hand1)
	tPair2 		= isTwoPair(hand2)
	onePair1 	= isOnePair(hand1)
	onePair2 	= isOnePair(hand2)

	if(rFlush1):
		return "Player 1"
	elif(rFlush2):
		return "Player 2"
	elif(fKind1 or fKind2):
		if(fKind1 == fKind2):
			if(high_card_hand == hand1):
				return "Player 1"
			else:
				return "Player 2"
		elif(fKind1):
			return "Player 1"
		elif(fKind2):
			return "Player 2"
		
	elif(sFlush1 or sFlush2):
		if(sFlush1 == sFlush2):
			if(high_card_hand == hand1):
				return "Player 1"
			else:
				return "Player 2"
		elif(sFlush1):
			return "Player 1"
		elif(sFlush2):
			return "Player 2"
		
	elif(fHouse1 or fHouse2):
		if(fHouse1 == fHouse2):
			g_test += 1 
			hand = largerFullhouse(hand1, hand2)

			if(hand == hand1):
				return "Player 1"
			else:
				return "Player 2"
		elif(fHouse1):
			return "Player 1"
		elif(fHouse2):
			return "Player 2"

		
	elif(straight1 or straight2):
		if(straight1 == straight2):
			if(high_card_hand == hand1):
				return "Player 1"
			else:
				return "Player 2"
		elif(straight1):
			return "Player 1"
		elif(straight2):
			return "Player 2"

		
	elif(tKind1 or tKind2):
		if(tKind1 == tKind2):
			if(high_card_hand == hand1):
				return "Player 1"
			else:
				return "Player 2"
		elif(tKind1):
			return "Player 1"
		elif(tKind2):
			return "Player 2"
		
		
	elif(tPair1 or tPair2):
		if(tPair1 == tPair2):
			if(high_card_hand == hand1):
				return "Player 1"
			else:
				return "Player 2"
		elif(tPair1):
			return "Player 1"
		elif(tPair2):
			return "Player 2"
		
	elif(onePair1 or onePair2):	
		if(onePair1 == onePair2):
			if(valueOfPair(hand1) > valueOfPair(hand2)):
				return "Player 1"
			else:
				return "Player 2"
		elif(onePair1):
			return "Player 1"
		elif(onePair2):
			return "Player 2"
		
	
	elif(highCard == hand1):
		return "Player 1"
	else:
		return "Player 2"

fd = open("../Data/winner.txt", 'w')

count = 0
with open("../Data/poker.txt", 'r') as f:
	for line in f:
		hand = line.split()
		player_one = hand[:5]
		player_two = hand[5:]
		
			
		

		handone = handlist(player_one)
		handtwo = handlist(player_two)

		handone.sort()
		handtwo.sort()
		#print(handone[0].__str__())
		#print(handone[1].__str__())
		#print(handone[2].__str__())
		#print(handone[3].__str__())
		#print(handone[4].__str__())
		#print(handtwo[0].__str__())
		#print(handtwo[1].__str__())
		#print(handtwo[2].__str__())
		#print(handtwo[3].__str__())
		#print(handtwo[4].__str__())
		
		s = compareHands(handone, handtwo)
		fd.write(str(player_one))
		fd.write(str(player_two))
		s = str(s)
		fd.write(s)
		fd.write("\n")
		if(s == "Player 1"):
			count += 1

f.close()
fd.close()
print(count)

