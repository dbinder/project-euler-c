#Problem 48
#The series, 11 + 22 + 33 + ... + 1010 = 10405071317.
#
#Find the last ten digits of the series, 11 + 22 + 33 + ... + 10001000.
#
#

import math

sums = 0
for i in range(1,11):
	sums += pow(i,i)

print sums
# 10405071317
sums = 0
for i in range(1,1000):
	sums += pow(i,i)
last = []

for i in range(10):
	last.append(sums % 10)
	sums = sums / 10

print last
