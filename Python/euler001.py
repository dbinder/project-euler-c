import time

temp = 0
f = open("../Data/timing_output.txt", "a")

start = time.clock()

for i in range(0,1000):
	if i % 5 == 0 or i % 3 == 0:
		temp += i
end = time.clock()

f.write("Problem 1 took %0.9f ms in Python\n" % ((end - start)*1000))
f.close()

print "The sum is %d" % temp