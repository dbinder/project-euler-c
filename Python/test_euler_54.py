#test euler 54.
import euler054
#fix this


################TESTS###############
CARDS = "23456789TJQKA"
CARDS_VALUES = dict(zip(CARDS, range(2,15)))

print("############ TEST list sort #########")
hand2 = [(10,"Club"), (9, "Diamond"), (9, "Club"), (11, "Club"), (7, "Diamond")]
hand2.sort()
print(hand2)


print("################ TEST Dictionary ####################")
hand1 = ["TC", "8C", "9C", "JC", "7D"]
temp = hand1[0]
temp2 = CARDS_VALUES.get(temp[0:1])
print(CARDS_VALUES)
print(CARDS_VALUES.get('A'))
print ("temp2 = ", temp2)
#temp = map(suit(x), in range hand1)
#print(temp)





print("############ TEST highCard #########")
hand1 = ["6D", "7C", "5D", "5H", "3S"]
hand2 = ["5C", "JC", "2H", "5S", "3D"]
hands1 = euler054.handlist(hand1)
hands2 = euler054.handlist(hand2)
s = euler054.highCard(hands1)
print(s.value)
s = euler054.highCard(hands2)
print(s.value)
print("############ TEST comepareHighCards #########")
hand1 = ["6D", "7C", "5D", "5H", "3S"]
hand2 = ["5C", "JC", "2H", "5S", "3D"]
hands1 = euler054.handlist(hand1)
hands2 = euler054.handlist(hand2)
hands1.sort()
hands2.sort()
s = euler054.comepareHighCards(hands1, hands2)
print(s[0].value)

print("############ TEST isFlush#########")
hand = ["5C","6C","7C","8C","9C"]
hands = euler054.handlist(hand)
s = euler054.isFlush(hands)
print(s)
hand = ["5C","6D","7C","8C","9C"]
hands = euler054.handlist(hand)
s = euler054.isFlush(hands)
print(s)

print("############ TEST isStraight #########")
hand = ["5C","8C","9C","6C","7D"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isStraight(hands)
print(s)
hand = ["TC","8C","9C","6C","7D"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isStraight(hands)
print(s)
hand = ["TC","8C","9C","JC","7D"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isStraight(hands)
print(s)
hand = ["TC","8C","9C","JC","QD"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isStraight(hands)
print(s)
hand = ["6C","8C","9C","JC","QD"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isStraight(hands)
print(s)

print("############ test isStraightFlush #########")
hand = ["5C","8C","9C","6C","7C"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isStraighFlush(hands)
print(s)
hand = ["5C","8C","9C","6C","7D"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isStraighFlush(hands)
print(s)

print("############ test isFourOfAKind #########")
hand = ["3H","3C","3S","6C","3D"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isFourOfAKind(hands)
print(s)
hand = ["3H","3C","4S","6C","3D"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isFourOfAKind(hands)
print(s)

print("############ test isThreeOfAKind #########")
hand = ["3H","3C","3S","6C","3D"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isThreeOfAKind(hands)
print(s)
hand = ["3H","3C","4S","6C","3D"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isThreeOfAKind(hands)
print(s)
hand = ["2H","3C","4S","6C","3D"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isThreeOfAKind(hands)
print(s)

print("############ test isTwoPair #########")
hand = ["3H","3C","3S","6C","3D"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isTwoPair(hands)
print(s)
hand = ["3H","3C","4S","6C","3D"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isTwoPair(hands)
print(s)
hand = ["2H","3C","2S","6C","3D"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isTwoPair(hands)
print(s)
hand = ['2C', '6D', '9S', 'KC', 'KS']
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isTwoPair(hands)
print(s)
hand = ['9D', '9H', 'QH', 'TC', 'TS']
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isTwoPair(hands)
print(s)
hand = ['5H', '6H', '7C', '7S', 'AS']
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isTwoPair(hands)
print(s)
hand = ['3D', '5C', '5D', 'KC', 'KH']
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isTwoPair(hands)
print(s)


print("############ test valueOfpair #########")
hand = ["3H","3C","3S","6C","3D"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.valueOfPair(hands)
print(s)
hand = ["3H","3C","4S","6C","3D"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.valueOfPair(hands)
print(s)
hand = ["2H","3C","2S","6C","3D"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.valueOfPair(hands)
print(s)
hand = ['2C', '6D', '9S', 'KC', 'KS']
hands = euler054.handlist(hand)
hands.sort()
s = euler054.valueOfPair(hands)
print(s)
hand = ['9D', '9H', 'QH', 'TC', 'TS']
hands = euler054.handlist(hand)
hands.sort()
s = euler054.valueOfPair(hands)
print(s)
hand = ['5H', '6H', '7C', '7S', 'AS']
hands = euler054.handlist(hand)
hands.sort()
s = euler054.valueOfPair(hands)
print(s)
hand = ['3D', '5C', '5D', 'KC', 'KH']
hands = euler054.handlist(hand)
hands.sort()
s = euler054.valueOfPair(hands)
print(s)


print("############ test isRoyalFlush #########")
hand = ["3H","3C","3S","6C","3D"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isRoyalFlush(hands)
print(s)
hand = ["3H","3C","4S","6C","3D"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isRoyalFlush(hands)
print(s)
hand = ["TS","JH","QH","KH","AH"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isRoyalFlush(hands)
print(s)
hand = ["TH","JH","QH","KH","AH"]
hands = euler054.handlist(hand)
hands.sort()
s = euler054.isRoyalFlush(hands)
print(s)

print("############ test compareHands #########")
handone = ["3H","3C","3S","6C","3D"]
handone = euler054.handlist(handone)
handone.sort()
handtwo = ["3H","3C","4S","6C","3D"]
handtwo = euler054.handlist(handtwo)
handtwo.sort()
s =  euler054.compareHands(handone, handtwo)
print(s)
handone = ["3H","3C","3S","6C","3D"]
handone = euler054.handlist(handone)
handone.sort()
handtwo = ["4H","4C","4S","6C","4D"]
handtwo = euler054.handlist(handtwo)
handtwo.sort()
s =  euler054.compareHands(handone, handtwo)
print(s)

print("############ test largerFullhouse #########")
handone = ["3H","3C","3S","6C","3D"]
handone = euler054.handlist(hand1)
handone.sort()
handtwo = ["3H","3C","4S","6C","3D"]
handtwo = euler054.handlist(hand2)
handtwo.sort()
s =  euler054.largerFullhouse(handone, handtwo)
print(s)

