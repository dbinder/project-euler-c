import time

def largestPrimeFactor(n):
	m_largestFactor = 1
	m_p = 3
	m_div = n
	
	while((m_div % 2) == 0):
		m_largestFactor = 2
		m_div = m_div / 2
		

	while(m_div != 1):
		while((m_div % m_p) == 0):
			m_largestFactor = m_p
			m_div = m_div / m_p
		m_p += 2
		

		
	return m_largestFactor
f = open("../Data/timing_output.txt", "a")

start = time.clock()
a = largestPrimeFactor(600851475143) 
end = time.clock()

f.write("Problem 3 took %0.9f ms in Python\n" % ((end - start)*1000))
f.close()
print a