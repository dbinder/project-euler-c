# 
# Dylan Binder
# Problem 19 - Project Euler
# https://projecteuler.net#
#You are given the following information, but you may prefer to do some research for yourself.

# 1 Jan 1900 was a Monday.
# Thirty days has September,
# April, June and November.
# All the rest have thirty-one,
# Saving February alone,
# Which has twenty-eight, rain or shine.
# And on leap years, twenty-nine.
# A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
# How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?


#Stubbing out problem 19 
# (Formula d + m + y/100 + (y/400) + c) mod 7
# Integer division for the year. 
# d is day of the month
# m is month from the month table
# y is the last two digits of the year
# c is the century numberc is the century number.
#For a Gregorian date, this is 6 if the first two digits of the year are evenly divisible by 4,
# and subsequent centuries are 4-2-0 (so the century numbers for 2000, 2100, 2200, and 2300 are respectively 6, 4, 2, and 0). For a Julian date, this is 6 for 1200, and subsequent centuries subtract 1 until 0, when the next century is 6 (so 1300 is 5, and 1100 is 0).

#Days Table
# 07 14 21 28    0 Sunday
# 01 08 15 22 29 1 Monday
# 02 09 16 23 30 2 Tuesday
# 03 10 17 24 31 3 Wednesday
# 04 11 18 25    4 Thursday
# 05 12 19 26    5 Friday
# 06 13 20 27    6 Saturday


#Month 		m Leap years
#January	0 -1
#February	3 2
#March		3
#April		6
#May 		1
#June      	4
#July 		6
#August		2
#Septemper	5
#October	0
#November	3
#December	5
def DayOfWeek(day, month, year, century):
		
	d = (day + month + year%100 + (year%100)/4 + c) % 7
	return d

def GetCenturyNumber(year):
	
	year = year / 100
	year = year % 4
	if year == 0:
		c = 6
		return c
	elif year == 1:
		c = 4
		return c
	elif year == 2:
		c = 2
		return c
	else:
		c = 0
		return c

def GetLeapYear(year):
	if (year % 4 == 0):
		return True
	elif (year % 400 == 0):
		return True
	else:
		return False

def GetMonth(month, year):
	m = [0,3,3,6,1,4,6,2,5,0,3,5]

	if(GetLeapYear(year)):
		if month == 1:
			return -1
		elif month == 2:
			return 2
	
	month -= 1
	return m[month]



# Jan 31, 2009
print (31 + 0 + 9 + 9/4 + 6) % 7
print("19 mod 10= ", 19 % 10)
print("1999 mod 100= ", 1999 % 100)
print("Leap year", GetLeapYear(2000))
print("Leap year", GetLeapYear(1996))
print("Leap year", GetLeapYear(1995))
# Jan 31, 1999
#print(DayOfWeek(31,0,1999))
print(31 + 0 + 99 + 99/4) % 7

#Aug 12, 1980
print(12 + 2 + 80 + 80/4) % 7
#Jan 1 2000
print(1 + (-1) + 0) % 7 
#Jan 1 1900
print(1 + 0 + 0 + 0 + 0)% 7

#Testing CenturyNumber
print(GetCenturyNumber(1600))
print(GetCenturyNumber(1700))
print(GetCenturyNumber(1800))
print(GetCenturyNumber(1900))
print(GetCenturyNumber(2000))

#Testing Month
print(GetMonth(1,2000))
print(GetMonth(2,2000))
print(GetMonth(3,2000))
print(GetMonth(4,2000))
print(GetMonth(1,1999))
print(GetMonth(2,1999))
print(GetMonth(3,1999))
print(GetMonth(4,1999))
print(GetMonth(5,1999))
print(GetMonth(6,1999))
#Years
count = 0
for i in range(1901, 2001):
	#months
	for j in range(1,13):
		#days
		for k in range(1,32):
		#make cases for February and the Leap year
			if (j == 2 and GetLeapYear(i) and k == 30):
				break
			elif (j == 2 and GetLeapYear(i) and k == 29):
				break
			else:
				c = GetCenturyNumber(i)
				year = i
				day = k
				month = GetMonth(j, year)
				dow = DayOfWeek(day, month, year, c)
				
				if (dow == 0 and k == 1):
					count += 1
					

print "Count = ", count