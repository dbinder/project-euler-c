import time


#bad performance
def fibonacciRecursive(n):
	if n == 0:
		return 0
	if n == 1: 
		return 1
	else:
		return (fibonacciRecursive(n-1) + fibonacciRecursive(n-2))
		#bad performance

def fib(n):
	result = []
	a, b = 0, 1
	while b < n:
		result.append(b)
		a, b = b, a + b
	return result
f = open("../Data/timing_output.txt", "a")
start = time.clock()

total = 0
#was not giving the performance I liked. 
#for i in range(0, 4000000):
#total = total + (fibonacciRecursive(4000000) % 2 == 0)
#	total = total + fibonacciRecursive()
		#print fibonacciRecursive(i)
temp = fib(4000000)
for s in temp:
	if s % 2 == 0:	
		total = total + s	
	
end = time.clock()

f.write("Problem 2 took %0.9f ms in Python\n" % ((end - start)*1000))
f.close()
print "Answer to problem 2 Project Euler = %d" % total
print "Time it took to compute the answer in ms = %0.9f" % ((end-start) * 1000)

