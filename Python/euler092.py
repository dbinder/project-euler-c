# 
# Dylan Binder
# Problem 22 - Project Euler
# https://projecteuler.net#
#


##Problem 92
#A number chain is created by continuously adding the square of the digits in a number to form a new number until it has been seen before.
#
#For example,
#
#44 to 32 to  13 to  10 to  1  1
#85 89 145 42 20  4 16 37  58  89
#
#Therefore any chain that arrives at 1 or 89 will become stuck in an endless loop. What is most amazing is that EVERY starting number will eventually arrive at 1 or 89.
#
#How many starting numbers below ten million will arrive at 89?
import math


#for i in range(1000000, 0, -1):

#lets make a list of digits
#sqaure each digit in the list
#add the list = 1 or 89 break
# = 89 ++count
#Make new list of from the sum. 
g_count = 0

def fillList(n):
	digits = []
	while(n > 0):
		i = n % 10
		digits.append(i)
		n = n / 10
	return digits	

def squareList(n):
	b = [x ** 2 for x in n]
	return b

def sumList(n):
	return sum(n)


for i in range(10000000, 0, -1):
	
	b = fillList(i)
	
	
	b = squareList(b)

	
	a = sum(b)
	while (a != 1):
		b = fillList(a)
		b = squareList(b)
		a = sum(b)
		
		if a == 89:
			g_count += 1
			break

print g_count