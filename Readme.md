#Readme

This is getting a little overwhelming with all the files.

#Stage 1:
At first I had all the files in a seperate directory. 

This was getting a little out of control and having a make file for each problem started to seem silly.

#Stage 2: 
Getting rid of the folders and renameing all ouf the c files to eulerXXX.c to clarify it a little bit. 

Again this is getting a little out of hand and I am also beginning to reuse code, for other problems. 


#Stage 3: 
Going to make one program that will call whatever euler problem I wish
I will put the function prototypes in euler.h, and define the functions in eulerp.c, the engine will be euler.c
Have not gotten to stage 3 yet.  Still most of this is a mess. 

All Data will go in a Data\folder
will have the name of the problem as the .txt file

###1/27/14:
Update.  Adding new directories for C++ and Python. I am going to be rewriting these problems in those languages. Since I moved the source to the C directory I suppose a bunch of the problems will not work due the hard coded locations of where the source is looking for data. 

I aslo redid problem 1 in cpp and in Python. I am going to go back and add timeing code.  And add an out put to each problem that so I will write the problem what language and the total time it took to compute the answer.

###1/29/2014:
Added a few python solutions to the over all python set.  Still need to go back and add all the timing code to the c section for all the problems. 

###2/13/2014
Finished problem 19.  That one is pretty straight forward. Also Finished problem 20, also problem 22. 

###2/18/2014
Finished problem 54. I have met my goal of finishing 25 problems.  Now I need to clean up and time them all, and translate them to C/C++/Python. Problem 25 needs major clean up. 

#Goal:
Complete. 

25/25:

